import { BrowserModule } from '@angular/platform-browser';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';

import { AppComponent } from './app.component';
import { APP_BASE_HREF } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { CommonLibraryModule } from 'commonLibrary';
import { OneuiCoreModule } from '@vnpt/oneui-core';

import { RouterModule, Routes } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';

import { VSpinModule } from '@vnpt/oneui-ui/spin';
import { VAlertModule } from '@vnpt/oneui-ui/alert';

const routes: Routes = [{ path: '**', component: EmptyRouteComponent }];

@NgModule({
  declarations: [AppComponent],
  imports: [
    RouterModule.forRoot(routes),
    NoopAnimationsModule,
    BrowserModule,
    CommonLibraryModule,
    OneuiCoreModule,
    VSpinModule,
    VAlertModule,
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {}
